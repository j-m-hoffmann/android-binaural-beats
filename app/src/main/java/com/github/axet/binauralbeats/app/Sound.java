package com.github.axet.binauralbeats.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.preference.PreferenceManager;

public class Sound extends com.github.axet.androidlibrary.sound.Sound {

    public static final int DEFAULT_CHANNELS = AudioFormat.CHANNEL_OUT_STEREO;
    public static final int DEFAULT_STREAM = AudioManager.STREAM_MUSIC;  // AudioSystem.STREAM_MUSIC == AudioManager.STREAM_MUSIC;
    public static final int DEFAULT_TYPE = AudioAttributes.CONTENT_TYPE_MUSIC;
    public static final int DEFAULT_USAGE = AudioAttributes.USAGE_MEDIA;
    public static final int DEFAULT_BUF = 1;

    public static int getAudioRate(Context context) {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        int rate = Integer.parseInt(shared.getString(BeatsApplication.PREFERENCE_RATE, "" + DEFAULT_RATE));
        rate = getValidAudioRate(DEFAULT_CHANNELS, rate);
        return rate;
    }

    public Sound(Context context) {
        super(context);
    }

    public void silent() {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        if (shared.getBoolean(BeatsApplication.PREFERENCE_SILENT, false)) {
            super.silent();
        }
    }

    public void unsilent() {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        if (shared.getBoolean(BeatsApplication.PREFERENCE_SILENT, false)) {
            super.unsilent();
        }
    }
}
