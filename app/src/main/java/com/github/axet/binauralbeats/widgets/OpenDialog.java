package com.github.axet.binauralbeats.widgets;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.github.axet.androidlibrary.app.Storage;
import com.github.axet.androidlibrary.widgets.ErrorDialog;
import com.github.axet.binauralbeats.R;
import com.github.axet.binauralbeats.activities.MainActivity;
import com.github.axet.binauralbeats.beats.Program;

import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.nio.charset.Charset;

public class OpenDialog extends DialogFragment {

    View view;

    public class Result implements DialogInterface {

        public Program pr;
        public boolean done;

        @Override
        public void cancel() {
        }

        @Override
        public void dismiss() {
        }
    }

    Result result = new Result();

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getContext());

        view = inflater.inflate(R.layout.recording, null, false);

        View col = view.findViewById(R.id.recording_collapsed);
        col.setVisibility(View.GONE);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(view);
        builder.setPositiveButton(R.string.start, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                result.done = true;
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                load();
            }
        });
        return dialog;
    }

    void load() {
        Bundle args = getArguments();

        Uri uri = (Uri) args.get("url");

        TextView title = (TextView) view.findViewById(R.id.recording_title);
        TextView desc = (TextView) view.findViewById(R.id.recording_desc);
        TextView desc2 = (TextView) view.findViewById(R.id.recording_desc2);
        LineGraphView line = (LineGraphView) view.findViewById(R.id.recording_player_seek);
        View play = view.findViewById(R.id.beats_player_play);
        View stop = view.findViewById(R.id.beats_player_stop);

        play.setVisibility(View.GONE);
        stop.setVisibility(View.GONE);

        ContentResolver resolver = getContext().getContentResolver();
        try {
            InputStream is = resolver.openInputStream(uri);
            String xml = IOUtils.toString(is, Charset.defaultCharset());
            Program p2 = Program.fromGnauralFactory(xml);

            if (p2 == null)
                throw new RuntimeException("unable to read " + Storage.getDisplayName(getContext(), uri));

            String t = p2.getName();
            if (t == null || t.isEmpty())
                t = "No Title";

            String d = p2.getDescription();
            if (d == null || d.isEmpty())
                d = "No Description";

            title.setText(t);
            desc.setText(d);
            desc2.setText(d);
            line.load(p2);

            result.pr = p2;
        } catch (Throwable e) {
            ErrorDialog.Error(getActivity(), e);
            dismiss();
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Activity a = getActivity();
        if (a instanceof DialogInterface.OnDismissListener) {
            ((DialogInterface.OnDismissListener) a).onDismiss(result);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
