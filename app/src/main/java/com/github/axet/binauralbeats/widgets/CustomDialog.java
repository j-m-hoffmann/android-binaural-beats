package com.github.axet.binauralbeats.widgets;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.axet.androidlibrary.widgets.CircularSeekBar;
import com.github.axet.binauralbeats.R;
import com.github.axet.binauralbeats.app.BeatsApplication;
import com.github.axet.binauralbeats.app.Sound;
import com.github.axet.binauralbeats.beats.SoundLoop;
import com.github.axet.binauralbeats.beats.TonePlayer;

public class CustomDialog extends DialogFragment {

    public static final float BEAT_MAX = 70f;
    public static final float BEAT_MIN = 0.1f;
    public static final float BASE_MAX = 1500f;

    public static final float DEFAULT_BG_VOL = 0.7f;

    public class Result implements DialogInterface {
        public long duration;
        public SoundLoop background;
        public boolean beats;
        public float base;
        public float beatStart;
        public float beatEnd;

        @Override
        public void cancel() {
        }

        @Override
        public void dismiss() {
        }
    }

    int hours; // minutes
    int minutes; // minutes

    float base;
    float beatStart;
    float beatEnd;

    TextView durText;
    CircularSeekBar dur;
    TextView startText;
    CircularSeekBar start;
    CircularSeekBar end;
    TextView endText;
    SwitchCompat beatsOn;
    SoundLoop background;
    TextView baseText;

    RadioButton beats_white;
    RadioButton beats_unity;

    Result result;

    TonePlayer tone;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getContext());

        tone = new TonePlayer(getContext());
        tone.start();
        tone.setVolume(Sound.log1(BeatsApplication.getFlatBtVol(getContext())));

        Bundle args = getArguments();

        long diff = args.getLong("duration");

        minutes = (int) (diff / (60 * 1000) % 60) * 60 * 1000;
        hours = (int) (diff / (60 * 60 * 1000) % 24) * 60 * 60 * 1000;
        boolean beats = args.getBoolean("beats");
        base = args.getFloat("base", 420f);
        beatStart = args.getFloat("start", 20f);
        beatEnd = args.getFloat("end", 20f);
        background = SoundLoop.values()[args.getInt("background")];

        View view = inflater.inflate(R.layout.custom, null, false);

        beats_white = (RadioButton) view.findViewById(R.id.beats_white);
        beats_unity = (RadioButton) view.findViewById(R.id.beats_unity);

        switch (background) {
            case UNITY:
                beats_unity.setChecked(true);
                break;
            case WHITE_NOISE:
                beats_white.setChecked(true);
                break;
        }

        durText = (TextView) view.findViewById(R.id.beats_duration_text);

        dur = (CircularSeekBar) view.findViewById(R.id.beats_duration);
        dur.setMax(59);
        dur.setProgress(minutes / 60 / 1000);
        dur.setOnSeekBarChangeListener(new CircularSeekBar.OnCircularSeekBarChangeListener() {
            boolean tracking = false; // changed two values continusly
            int old;

            @Override
            public void onProgressChanged(CircularSeekBar circularSeekBar, int progress, boolean fromUser) {
                if (tracking) {
                    if (old > 40 && progress < 20) {
                        hours += 60 * 60 * 1000;
                    }
                    if (old < 20 && progress > 40) {
                        hours -= 60 * 60 * 1000;
                    }
                    if (hours < 0)
                        hours = 0;
                }
                minutes = progress * 60 * 1000;

                updateText();

                old = progress;
                tracking = true;
            }

            @Override
            public void onStopTrackingTouch(CircularSeekBar seekBar) {
                tracking = false;
            }

            @Override
            public void onStartTrackingTouch(CircularSeekBar seekBar) {
            }
        });

        baseText = (TextView) view.findViewById(R.id.base_text);
        final SeekBar basePr = (SeekBar) view.findViewById(R.id.base_progress);
        basePr.setMax((int) (BASE_MAX * 10) - 1);
        basePr.setProgress((int) (base * 10 - 1));
        basePr.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                base = (progress + 1) / 10f;
                updateText();
                tone.setBase(beatStart, base);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                tone.toneStart(beatStart, base);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                tone.toneEnd();
            }
        });
        View plusBase = view.findViewById(R.id.button_plus_base);
        plusBase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                base += 0.1;
                if (base > BASE_MAX)
                    base = BASE_MAX;
                basePr.setProgress((int) (base * 10 - 1));
                updateText();
                tone.tonePulse(beatStart, base);
            }
        });
        View minusBase = view.findViewById(R.id.button_minus_base);
        minusBase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                base -= 0.1;
                if (base < BEAT_MIN)
                    base = BEAT_MIN;
                basePr.setProgress((int) (base * 10 - 1));
                updateText();
                tone.tonePulse(beatStart, base);
            }
        });

        beatsOn = (SwitchCompat) view.findViewById(R.id.beats_on);
        beatsOn.setChecked(beats);

        View plusStart = view.findViewById(R.id.button_plus_start);
        plusStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beatStart += 0.1;
                if (beatStart > BEAT_MAX)
                    beatStart = BEAT_MAX;
                start.setProgress((int) (beatStart * 10 - 1));
                updateText();
                tone.tonePulse(beatStart, base);
            }
        });
        View minusStart = view.findViewById(R.id.button_minus_start);
        minusStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beatStart -= 0.1;
                if (beatStart < BEAT_MIN)
                    beatStart = BEAT_MIN;
                start.setProgress((int) (beatStart * 10 - 1));
                updateText();
                tone.tonePulse(beatStart, base);
            }
        });
        startText = (TextView) view.findViewById(R.id.beats_start_text);
        start = (CircularSeekBar) view.findViewById(R.id.beats_start);
        start.setMax((int) (BEAT_MAX * 10 - 1));
        start.setProgress((int) (beatStart * 10 - 1));
        start.setOnSeekBarChangeListener(new CircularSeekBar.OnCircularSeekBarChangeListener() {
            @Override
            public void onProgressChanged(CircularSeekBar circularSeekBar, int progress, boolean fromUser) {
                beatStart = (progress + 1) / 10f;
                updateText();
                tone.setBase(beatStart, base);
            }

            @Override
            public void onStopTrackingTouch(CircularSeekBar seekBar) {
                tone.toneEnd();
            }

            @Override
            public void onStartTrackingTouch(CircularSeekBar seekBar) {
                beatsOn.setChecked(true);
                tone.toneStart(beatStart, base);
            }
        });

        View plusEnd = view.findViewById(R.id.button_plus_end);
        plusEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beatEnd += 0.1;
                if (beatEnd > BEAT_MAX)
                    beatEnd = BEAT_MAX;
                end.setProgress((int) (beatEnd * 10 - 1));
                updateText();
                tone.tonePulse(beatEnd, base);
            }
        });
        View minusEnd = view.findViewById(R.id.button_minus_end);
        minusEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beatEnd -= 0.1;
                if (beatEnd < BEAT_MIN)
                    beatEnd = BEAT_MIN;
                end.setProgress((int) (beatEnd * 10 - 1));
                updateText();
                tone.tonePulse(beatEnd, base);
            }
        });
        endText = (TextView) view.findViewById(R.id.beats_end_text);
        end = (CircularSeekBar) view.findViewById(R.id.beats_end);
        end.setMax((int) (BEAT_MAX * 10 - 1));
        end.setProgress((int) (beatEnd * 10 - 1));
        end.setOnSeekBarChangeListener(new CircularSeekBar.OnCircularSeekBarChangeListener() {
            @Override
            public void onProgressChanged(CircularSeekBar circularSeekBar, int progress, boolean fromUser) {
                beatEnd = (progress + 1) / 10f;
                updateText();
                tone.setBase(beatEnd, base);
            }

            @Override
            public void onStopTrackingTouch(CircularSeekBar seekBar) {
                tone.toneEnd();
            }

            @Override
            public void onStartTrackingTouch(CircularSeekBar seekBar) {
                beatsOn.setChecked(true);
                tone.toneStart(beatEnd, base);
            }
        });

        updateText();

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(view);
        builder.setPositiveButton(R.string.start, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                result = new Result();
                result.duration = hours + minutes;
                result.beats = beatsOn.isChecked();
                result.base = base;
                result.beatStart = beatStart;
                result.beatEnd = beatEnd;
                result.background = SoundLoop.NONE;
                if (beats_white.isChecked())
                    result.background = SoundLoop.WHITE_NOISE;
                if (beats_unity.isChecked())
                    result.background = SoundLoop.UNITY;
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        AlertDialog dialog = builder.create();
        return dialog;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Activity a = getActivity();
        if (a instanceof DialogInterface.OnDismissListener) {
            ((DialogInterface.OnDismissListener) a).onDismiss(result);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        tone.shutdown();
    }

    void updateText() {
        long diff = hours + minutes;

        int diffMinutes = (int) (diff / (60 * 1000) % 60);
        int diffHours = (int) (diff / (60 * 60 * 1000) % 24);

        String str = "";

        if (diffHours > 0)
            str = BeatsApplication.formatTime(diffHours) + getString(R.string.h) + " " + BeatsApplication.formatTime(diffMinutes) + getString(R.string.m);
        else
            str = BeatsApplication.formatTime(diffMinutes) + getString(R.string.mins);

        durText.setText(str);

        String f = "%.1f Hz";
        endText.setText(String.format(f, beatEnd));
        startText.setText(String.format(f, beatStart));

        baseText.setText(String.format(f, base));
    }
}
