package com.github.axet.binauralbeats.beats;

import android.content.Context;
import android.util.Log;

import com.github.axet.androidlibrary.sound.AudioTrack;
import com.github.axet.binauralbeats.app.Sound;

import java.util.ArrayList;
import java.util.Arrays;

public class VoicesPlayer extends Thread {
    private static final String TAG = VoicesPlayer.class.getSimpleName();

    public static final int MAX_VOICES = 10;
    public static final int FADE_PROGRAM = 1000; // fade in/out ms
    public static final int FADE_PERIOD = 2500; // fade in/out ms
    public static final float FADE_MIN = 0.6f;

    FloatSinTable sinT;

    AudioTrack track;
    long anglesL[] = new long[MAX_VOICES];
    long anglesR[] = new long[MAX_VOICES];

    boolean playing = false;

    final Object lock = new Object();

    float ws[];
    AudioTrack.AudioBuffer buf;

    float freq;

    protected Context context;
    protected State state = new State();

    public static class State extends WhiteNoise.State {
        ArrayList<BinauralBeatVoice> voices;

        public State() {
        }

        public State(State state) {
            super(state);
            voices = state.voices;
        }

        public State(ArrayList<BinauralBeatVoice> v) {
            voices = v;
            setInfinite();
        }
    }

    public VoicesPlayer(Context context) {
        super("VoicePlayer");
        this.context = context;
        int rate = Sound.getAudioRate(context);
        if (rate == -1)
            throw new RuntimeException("unable to initialize audio params"); // not supported by phone
        sinT = new FloatSinTable(rate);
        buf = new AudioTrack.AudioBuffer(rate, Sound.DEFAULT_CHANNELS, Sound.DEFAULT_AUDIOFORMAT);
        track = AudioTrack.create(Sound.DEFAULT_STREAM, Sound.DEFAULT_USAGE, Sound.DEFAULT_TYPE, buf, buf.len * Sound.DEFAULT_BUF);
        ws = new float[buf.buffer.length];
    }

    public void playVoices(ArrayList<BinauralBeatVoice> voices) {
        State state = new State(voices);
        play(state);
    }

    public void play(State state) {
        synchronized (lock) {
            this.state = state;

            playing = true;
            if (track.getPlayState() != AudioTrack.PLAYSTATE_PLAYING)
                track.play();

            lock.notifyAll();
        }
    }

    public void playVoices(long dur, ArrayList<BinauralBeatVoice> voices, boolean fadeStart, boolean fadeEnd) {
        State state = new State();
        state.setDur(buf.hz, dur);
        state.setFadeStart(fadeStart ? 0f : FADE_MIN, buf.hz, (fadeStart ? FADE_PROGRAM : FADE_PERIOD));
        state.setFadeEnd(fadeEnd ? 0f : FADE_MIN, buf.hz, (fadeEnd ? FADE_PROGRAM : FADE_PERIOD));
        state.voices = voices;
        play(state);
    }

    public void setVolume(float vol) {
        track.setStereoVolume(vol, vol);
    }

    public void setVolume(float l, float r) {
        track.setStereoVolume(l, r);
    }

    public void stopVoices() {
        playing = false;
        track.stop();
        anglesL = new long[MAX_VOICES];
        anglesR = new long[MAX_VOICES];
    }

    public void shutdown() {
        interrupt();
        try {
            join();
        } catch (InterruptedException e) {
            interrupt();
        }
        if (track != null) {
            track.release();
            track = null;
        }
    }

    public boolean moreSamples(State state) {
        if (state.dur > 0)
            return state.samplesNum < state.durSamples;
        else {
            return state.voices != null && state == this.state;
        }
    }

    @Override
    public void run() {
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO);

        State state = this.state;
        while (!isInterrupted()) {
            try {
                synchronized (lock) {
                    if (!moreSamples(state)) {
                        if (!next(state))
                            lock.wait();
                        state = this.state;
                    }
                    fillSamples(state);
                }
            } catch (InterruptedException e) {
                return;
            }
            while (buf.pos != buf.len) {
                int out = track.write(buf);

                if (out < 0) {
                    return;
                } else {
                    if (buf.pos != buf.len) {
                        try {
                            synchronized (lock) {
                                Log.d(TAG, "Locked");
                                lock.wait(); // track on pause
                            }
                        } catch (InterruptedException e) {
                            return;
                        } finally {
                            Log.d(TAG, "Unlocked");
                        }
                    }
                }

                if (isInterrupted())
                    return;
            }
        }
    }

    public void fillSamples(State state) {
        Arrays.fill(ws, 0);

        float fm = 0;
        int fc = 0;

        int ISCALE = sinT.size;
        int TwoPi = sinT.size;

        for (int j = 0; j < state.voices.size(); j++) { // freqs.length
            BinauralBeatVoice v = state.voices.get(j);
            float base_freq;

            if (BinauralBeatVoice.DEFAULT == v.pitch)
                base_freq = voice2Pitch(j);
            else
                base_freq = v.pitch;

            long angle1 = anglesL[j];
            long angle2 = anglesR[j];

            long pos = state.samplesNum;
            for (int i = 0; i < buf.len; i += 2) {
                float ratio = (v.freqEnd - v.freqStart) / state.durSamples;
                float freq = v.freqStart + ratio * pos;

                fm += freq;
                fc++;

                int inc1 = (int) (TwoPi * (base_freq + freq) / buf.hz);
                int inc2 = (int) (TwoPi * (base_freq) / buf.hz);

                ws[i] += sinT.sinFastInt(angle1) * v.volume;
                ws[i + 1] += sinT.sinFastInt(angle2) * v.volume;
                angle1 += inc1;
                angle2 += inc2;
                pos++;
            }

            anglesL[j] = angle1 % ISCALE;
            anglesR[j] = angle2 % ISCALE;
        }

        freq = fm / fc;

        for (int i = 0; i < buf.len; i += 2) {
            float vol = 1f;
            if (state.samplesNum < state.fadeInEnd) {
                float flat = state.samplesNum / (float) state.fadeStartSamples;
                vol = state.fadeStart + Sound.log1(flat) * (1f - state.fadeStart);
            }
            if (state.dur > 0) {
                if (state.samplesNum > state.fadeOutStart) {
                    float flat = (state.durSamples - state.samplesNum) / (float) state.fadeEndSamples;
                    if (flat < 0) // when samples buffer is bigger then fadeOut mute the rest
                        flat = 0;
                    vol = state.fadeEnd + Sound.log1(flat) * (1f - state.fadeEnd);
                }
            }
            buf.write(i, (short) (vol * ws[i] * Short.MAX_VALUE / state.voices.size()), (short) (vol * ws[i + 1] * Short.MAX_VALUE / state.voices.size()));
            state.samplesNum++;
        }

        buf.reset();
    }

    public Note voice2Note(int i) {
        switch (i) {
            case 0:
                return new Note(Note.NoteK.A);
            case 1:
                return new Note(Note.NoteK.C);
            case 2:
                return new Note(Note.NoteK.E);
            case 3:
                return new Note(Note.NoteK.G);
            case 4:
                return new Note(Note.NoteK.C, 5);
            case 5:
                return new Note(Note.NoteK.E, 6);
            default:
                return new Note(Note.NoteK.A, 7);
        }
    }

    public float voice2Pitch(int i) {
        Note n = voice2Note(i);
        return (float) n.getPitchFreq();
    }

    public boolean next(State state) {
        return state != this.state;
    }
}