package com.github.axet.binauralbeats.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import com.github.axet.androidlibrary.app.MainApplication;
import com.github.axet.binauralbeats.R;
import com.github.axet.binauralbeats.beats.BeatsPlayer;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class BeatsApplication extends MainApplication {
    public static final String PREFERENCE_RATE = "sample_rate";
    public static final String PREFERENCE_CALL = "call";
    public static final String PREFERENCE_SILENT = "silence";
    public static final String PREFERENCE_THEME = "theme";
    public static final String PREFERENCE_BT = "VOLUME_VC";
    public static final float PREFERENCE_BT_DEFAULT = BeatsPlayer.DEFAULT_VOLUME;
    public static final String PREFERENCE_BG = "VOLUME_BG";
    public static final float PREFERENCE_BG_DEFAULT = BeatsPlayer.DEFAULT_VOLUME * BeatsPlayer.BG_VOLUME_RATIO;

    public static final String PREFERENCE_OPTIMIZATION = "optimization";
    public static final String PREFERENCE_NEXT = "next";
    public static final String PREFERENCE_BOOT = "boot";
    public static final String PREFERENCE_INSTALL = "install";

    public static final String CUSTOM_BACKGROUND = "CUSTOM_BACKGROUND";
    public static final String CUSTOM_DURATION = "CUSTOM_DURATION";
    public static final String CUSTOM_BASE = "CUSTOM_BASE";
    public static final String CUSTOM_BEAT = "CUSTOM_BEAT";
    public static final String CUSTOM_BEATSTART = "CUSTOM_BEATSTART";
    public static final String CUSTOM_BEATEND = "CUSTOM_BEATEND";

    public static SimpleDateFormat APACHE_DATE = new SimpleDateFormat("dd-MMM-yyyy HH:mm", Locale.US);

    @Override
    public void onCreate() {
        super.onCreate();
        PreferenceManager.setDefaultValues(this, R.xml.pref_general, false);
    }

    public static int getTheme(Context context, int light, int dark) {
        return MainApplication.getTheme(context, PREFERENCE_THEME, light, dark, context.getString(R.string.Theme_Dark));
    }

    public static float getFlatBtVol(Context context) {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        return shared.getFloat(BeatsApplication.PREFERENCE_BT, PREFERENCE_BT_DEFAULT);
    }

    public static float getFlatBgVol(Context context) {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        return shared.getFloat(BeatsApplication.PREFERENCE_BG, PREFERENCE_BG_DEFAULT);
    }
}
