package com.github.axet.binauralbeats.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.AudioDeviceInfo;
import android.media.AudioRouting;
import android.media.AudioTrack;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.axet.androidlibrary.activities.AppCompatThemeActivity;
import com.github.axet.androidlibrary.animations.ExpandItemAnimator;
import com.github.axet.androidlibrary.preferences.AboutPreferenceCompat;
import com.github.axet.androidlibrary.preferences.OptimizationPreferenceCompat;
import com.github.axet.androidlibrary.widgets.ErrorDialog;
import com.github.axet.androidlibrary.widgets.HeaderRecyclerAdapter;
import com.github.axet.androidlibrary.widgets.OpenChoicer;
import com.github.axet.androidlibrary.widgets.OpenFileDialog;
import com.github.axet.androidlibrary.widgets.Toast;
import com.github.axet.binauralbeats.R;
import com.github.axet.binauralbeats.animations.BeatsAnimation;
import com.github.axet.binauralbeats.app.BeatsApplication;
import com.github.axet.binauralbeats.beats.BeatsPlayer;
import com.github.axet.binauralbeats.beats.BinauralBeatVoice;
import com.github.axet.binauralbeats.beats.DefaultProgramsBuilder;
import com.github.axet.binauralbeats.beats.Note;
import com.github.axet.binauralbeats.beats.Period;
import com.github.axet.binauralbeats.beats.Program;
import com.github.axet.binauralbeats.beats.ProgramMeta;
import com.github.axet.binauralbeats.beats.SoundLoop;
import com.github.axet.binauralbeats.services.BeatsService;
import com.github.axet.binauralbeats.widgets.CustomDialog;
import com.github.axet.binauralbeats.widgets.LineGraphView;
import com.github.axet.binauralbeats.widgets.OpenDialog;
import com.github.axet.binauralbeats.widgets.SoundTestDialog;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class MainActivity extends AppCompatThemeActivity implements DialogInterface.OnDismissListener {
    public final static String TAG = MainActivity.class.getSimpleName();

    public final static String STOP_BUTTON = MainActivity.class.getCanonicalName() + ".STOP_BUTTON";
    public final static String PAUSE_BUTTON = MainActivity.class.getCanonicalName() + ".PAUSE_BUTTON";

    public static final String[] PERMISSIONS = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};

    public static final SimpleDateFormat HHMMSS = new SimpleDateFormat("HH:mm:ss");

    public static final int RESULT_FILE = 1;

    public static final int TYPE_COLLAPSED = 0;
    public static final int TYPE_EXPANDED = 1;
    public static final int TYPE_DELETED = 2;

    public static final int PLAYING_CUSTOM = -2;
    public static final int PLAYING_NONE = -1;

    public static final int PLAYER_UPDATE = 1000;

    public static final int[] ALL = {TYPE_COLLAPSED, TYPE_EXPANDED};

    Handler handler = new Handler();
    RecyclerView list;
    ExpandItemAnimator animator;
    Recordings recordings;

    View fab_panel;
    View fab_stop;
    FloatingActionButton fab;

    BroadcastReceiver receiver;
    ScreenReceiver screenreceiver;
    PhoneStateChangeListener pscl = new PhoneStateChangeListener();

    OpenChoicer choicer;

    public static void startActivity(Context context) {
        Intent i = new Intent(context, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(i);
    }

    public static float getFloat(SharedPreferences shared, String key, float def) { // < 1.1.9
        try {
            return shared.getInt(key, (int) def);
        } catch (ClassCastException c) {
            return shared.getFloat(key, def);
        }
    }

    public static void boostVolume(Program p) {
        float max = 0;
        for (Iterator<Period> i = p.getPeriodsIterator(); i.hasNext(); ) {
            Period o = i.next();
            if (max < o.getBackgroundvol())
                max = o.getBackgroundvol();
            List<BinauralBeatVoice> bb = o.getVoices();
            if (bb != null) {
                for (BinauralBeatVoice b : bb) {
                    if (max < b.volume)
                        max = b.volume;
                }
            }
        }
        float v = 1 / max;
        for (Iterator<Period> i = p.getPeriodsIterator(); i.hasNext(); ) {
            Period o = i.next();
            o.setBackgroundvol(o.getBackgroundvol() * v);
            List<BinauralBeatVoice> bb = o.getVoices();
            if (bb != null) {
                for (BinauralBeatVoice b : bb) {
                    b.volume *= v;
                }
            }
        }
    }

    class PhoneStateChangeListener extends PhoneStateListener {
        public boolean wasRinging;
        public boolean pausedByCall;

        @Override
        public void onCallStateChanged(int s, String incomingNumber) {
            switch (s) {
                case TelephonyManager.CALL_STATE_RINGING:
                    wasRinging = true;
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    wasRinging = true;
                    if (recordings.player != null && recordings.player.isPlaying() && !recordings.player.isEnd()) {
                        recordings.playerPause();
                        pausedByCall = true;
                    }
                    break;
                case TelephonyManager.CALL_STATE_IDLE:
                    if (pausedByCall) {
                        recordings.playerPause();
                    }
                    wasRinging = false;
                    pausedByCall = false;
                    break;
            }
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View v) {
            super(v);
        }
    }

    public static class CategoryHolder extends ViewHolder {
        public TextView category;

        public CategoryHolder(View v) {
            super(v);
            category = (TextView) v.findViewById(R.id.category_text);
        }
    }

    public static class BeatsHolder extends ViewHolder {
        public View base;
        public TextView title;
        public TextView desc;
        public View ctrl;
        public ImageView play;
        public ImageView play2;
        public View stop;
        public View stop2;
        public View playerBase;
        public TextView desc2;
        public ImageView expand;
        public TextView status;
        public LineGraphView bar;
        public View minus;
        public View plus;
        public TextView loop_text;

        public BeatsHolder(View v) {
            super(v);
            base = v.findViewById(R.id.recording_base);
            title = (TextView) v.findViewById(R.id.recording_title);
            desc = (TextView) v.findViewById(R.id.recording_desc);
            play = (ImageView) v.findViewById(R.id.beats_player_play);
            stop = v.findViewById(R.id.beats_player_stop);
            ctrl = v.findViewById(R.id.beats_controls);
            if (ctrl != null) {
                play2 = (ImageView) ctrl.findViewById(R.id.beats_player_play);
                stop2 = ctrl.findViewById(R.id.beats_player_stop);
                minus = ctrl.findViewById(R.id.button_minus);
                plus = ctrl.findViewById(R.id.button_plus);
                loop_text = (TextView) ctrl.findViewById(R.id.loop_text);
            }
            playerBase = v.findViewById(R.id.recording_player);
            desc2 = (TextView) v.findViewById(R.id.recording_desc2);
            expand = (ImageView) v.findViewById(R.id.beats_expand);
            status = (TextView) v.findViewById(R.id.recording_player_status);
            bar = (LineGraphView) v.findViewById(R.id.recording_player_seek);
        }
    }

    public class Recordings extends RecyclerView.Adapter<ViewHolder> {
        Context context;

        BeatsPlayer player;
        Snackbar snackbar;

        int playing;
        BeatsHolder playingView; // current playing view

        Runnable updatePlayer;
        int selected = PLAYING_NONE;
        ProgramMeta.Category old;

        HashMap<ProgramMeta, Program> library = new HashMap<>();
        HashMap<ProgramMeta.Category, String> cats = new HashMap<>();
        HashMap<Program, Integer> counts = new HashMap<>();
        ArrayList<Object> list = new ArrayList<>();

        Toast toast;

        HeaderRecyclerAdapter empty;

        public Recordings(Context context) {
            this.context = context;
            setHasStableIds(true);
            empty = new HeaderRecyclerAdapter(this);
        }

        public void scan() {
            list.clear();

            load(ProgramMeta.Category.HYPNOSIS, R.string.group_hypnosis);
            load(ProgramMeta.Category.SLEEP, R.string.group_sleep);
            load(ProgramMeta.Category.HEALING, R.string.group_healing);
            load(ProgramMeta.Category.LEARNING, R.string.group_learning);
            load(ProgramMeta.Category.MEDITATION, R.string.group_mediation);
            load(ProgramMeta.Category.STIMULATION, R.string.group_simulation);
            load(ProgramMeta.Category.OOBE, R.string.group_oobe);
            load(ProgramMeta.Category.OTHER);

            load(R.string.program_morphine, "HEALING_MORPHINE");
            load(R.string.program_learning, "LEARNING_LEARNING");
            load(R.string.program_wakefulrelax, "MEDITATION_WAKEFULRELAX");
            load(R.string.program_schumann_resonance, "MEDITATION_SCHUMANN_RESONANCE");
            load(R.string.program_unity, "MEDITATION_UNITY");
            load(R.string.program_shamanic_rhythm, "MEDITATION_SHAMANIC_RHYTHM");
            load(R.string.program_powernap, "SLEEP_POWERNAP");
            load(R.string.program_sleep_induction, "SLEEP_SLEEP_INDUCTION");
            load(R.string.program_smr, "SLEEP_SMR");
            load(R.string.program_airplanetravelaid, "SLEEP_AIRPLANETRAVELAID");
            load(R.string.program_adhd, "STIMULATION_ADHD");
            load(R.string.program_hiit, "STIMULATION_HIIT");
            load(R.string.program_hallucination, "STIMULATION_HALLUCINATION");
            load(R.string.program_highest_mental_activity, "STIMULATION_HIGHEST_MENTAL_ACTIVITY");
            load(R.string.program_creativity, "STIMULATION_CREATIVITY");
            load(R.string.program_self_hypnosis, "HYPNOSIS_SELF_HYPNOSIS");
            load(R.string.program_lucid_dreams, "OOBE_LUCID_DREAMS");
            load(R.string.program_astral_01_relax, "OOBE_ASTRAL_01_RELAX");
            load(R.string.program_lucid_dreams_2, "OOBE_LUCID_DREAMS_2");

            for (ProgramMeta m : library.keySet()) {
                Program p = library.get(m);
                boostVolume(p);
            }

            notifyDataSetChanged();
        }

        void load(ProgramMeta.Category c) {
            cats.put(c, c.toString().toUpperCase());
        }

        void load(ProgramMeta.Category c, int id) {
            String n = getString(id);
            cats.put(c, n.toUpperCase());
        }

        void load(int id, String name) {
            String desc = getString(id);
            String[] ss = desc.split("\\|");
            Program p = new Program(ss[0]);
            Method m;
            try {
                m = DefaultProgramsBuilder.class.getDeclaredMethod(name, Program.class);
                p = (Program) m.invoke(null, p);
            } catch (NoSuchMethodException e) {
                try {
                    m = DefaultProgramsBuilder.class.getDeclaredMethod(name, Context.class, Program.class);
                    p = (Program) m.invoke(null, MainActivity.this, p);
                } catch (Exception ee) {
                    Log.d(TAG, "Load error", ee);
                    return;
                }
            } catch (Exception e) {
                Log.d(TAG, "Load error", e);
                return;
            }
            if (ss.length > 1)
                p.setDescription(ss[1]);
            ProgramMeta.Category c = DefaultProgramsBuilder.getMatchingCategory(name);
            ProgramMeta pm = new ProgramMeta(m, p.getName(), c);
            library.put(pm, p);
            if (old != c) {
                old = c;
                list.add(cats.get(c));
            }
            list.add(pm);
        }

        public void close() {
            playerStop();
            if (player != null) {
                player.release();
                player = null;
            }
            if (updatePlayer != null) {
                handler.removeCallbacks(updatePlayer);
                updatePlayer = null;
            }
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(context);
            switch (viewType) {
                case 0:
                    return new BeatsHolder(inflater.inflate(R.layout.recording, parent, false));
                case 1:
                    return new CategoryHolder(inflater.inflate(R.layout.category, parent, false));
            }
            return null;
        }

        @Override
        public void onBindViewHolder(final ViewHolder hh, int position) {
            Object o = list.get(position);

            if (o instanceof String) {
                CategoryHolder h = (CategoryHolder) hh;
                h.category.setText((String) o);
                return;
            }

            final BeatsHolder h = (BeatsHolder) hh;
            if (playing == position) {
                playingView = h;
            } else { // different index
                if (playingView == h) // reused view
                    playingView = null;
            }

            final ProgramMeta pm = (ProgramMeta) o;
            final Program pr = library.get(pm);

            h.title.setText(pm.getName());

            h.desc.setText(pr.getDescription() + " " + pr.getAuthor());

            final Runnable pl = new Runnable() {
                @Override
                public void run() {
                    if (player == null) {
                        playerPlay(h, pr, h.getAdapterPosition());
                    } else if (player.isPlaying()) {
                        int pos = h.getAdapterPosition();
                        if (playing != pos) {
                            playerStop();
                            notifyDataSetChanged(); // update previous player View
                            playerPlay(h, pr, pos);
                        } else {
                            playerPause(h, pr, pos);
                        }
                    } else {
                        int pos = h.getAdapterPosition();
                        if (playing != pos) {
                            playerStop();
                            notifyDataSetChanged(); // update previous player View
                        }
                        playerPlay(h, pr, pos);
                    }
                    updateCustom();
                }
            };

            h.play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pl.run();
                }
            });
            h.play2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pl.run();
                }
            });

            final Runnable st = new Runnable() {
                @Override
                public void run() {
                    playerStop();
                    notifyDataSetChanged(); // updatePlayerText(fview, pr, position); // does not update ctrls visibility
                }
            };

            h.stop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    st.run();
                }
            });
            h.stop2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    st.run();
                }
            });

            updatePlayerText(h, pr, position);

            h.playerBase.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

            if (selected == position) {
                updatePlayerText(h, pr, position);

                h.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        select(PLAYING_NONE);
                    }
                });

                h.desc2.setText(pr.getDescription() + " " + pr.getAuthor());
                h.desc2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        select(PLAYING_NONE);
                    }
                });
                h.expand.setImageResource(R.drawable.ic_expand_less_black_24dp);
            } else {
                h.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        select(h.getAdapterPosition());
                    }
                });
                h.expand.setImageResource(R.drawable.ic_expand_more_black_24dp);
            }

            animator.onBindViewHolder(h, position);
        }

        void playerPlay(final BeatsHolder h, final Program pr, final int position) {
            BeatsPlayer.PresetsPool old = null;
            if (player != null && player.isEnd()) {
                old = player.presets;
                player.presets = null;
                playerStop();
            }
            if (player == null) {
                player = new BeatsPlayer(context, pr, handler, old) {
                    @Override
                    public void onEnd() {
                        super.onEnd();
                        int count = getCount(pr);
                        if (count > 0) {
                            count--;
                            setCount(pr, count);
                            playerPlay(h, pr, position);
                        }
                    }
                };
            }
            player.setFlatBtVol(BeatsApplication.getFlatBtVol(MainActivity.this));
            player.setFlatBgVol(BeatsApplication.getFlatBgVol(MainActivity.this));

            player.play();

            if (Build.VERSION.SDK_INT >= 23) {
                final AudioTrack a = player.getTrack();
                final Runnable check = new Runnable() {
                    long last = AudioDeviceInfo.TYPE_UNKNOWN;

                    @TargetApi(23)
                    @Override
                    public void run() {
                        if (snackbar != null)
                            snackbar.dismiss();
                        AudioDeviceInfo adi = a.getRoutedDevice();
                        if (adi != null) {
                            int type = adi.getType();
                            if (type == AudioDeviceInfo.TYPE_BUILTIN_SPEAKER) {
                                if (last != AudioDeviceInfo.TYPE_UNKNOWN && last != AudioDeviceInfo.TYPE_BUILTIN_SPEAKER) {
                                    if (player != null && player.isPlaying()) {
                                        player.pause();
                                        Toast.makeText(context, getString(R.string.headphones_unplugged), Toast.LENGTH_LONG).show();
                                        return;
                                    }
                                }
                                View p = findViewById(android.R.id.content);
                                snackbar = Snackbar.make(p, getString(R.string.headphones_warning) + " " + getString(R.string.app_name), Snackbar.LENGTH_INDEFINITE);
                                snackbar.show();
                            }
                            last = type;
                        }
                    }
                };
                a.addOnRoutingChangedListener(new AudioTrack.OnRoutingChangedListener() {
                    @Override
                    public void onRoutingChanged(AudioTrack audioTrack) {
                        check.run();
                    }

                    @Override
                    public void onRoutingChanged(AudioRouting router) {
                        check.run();
                    }
                }, handler);
                check.run();
            }

            Recordings.this.playing = position;
            Recordings.this.playingView = h;

            updatePlayerRun(pr, position);

            BeatsService.startService(context, pr.getName(), formatPlayerStatus(), true, false);
        }

        void playerPause() {
            if (player == null)
                return;
            if (!player.isPlaying())
                playerPlay(playingView, player.getProgram(), playing);
            else
                playerPause(playingView, player.getProgram(), playing);
            notifyDataSetChanged();
        }

        String formatPlayerStatus() {
            if (player == null)
                return "";
            String s = player.getStatus();
            int c = getCount(player.getProgram());
            if (c > 0)
                s += String.format(" (x%d)", (c + 1));
            return s;
        }

        void playerPause(BeatsHolder h, Program pr, int position) {
            if (snackbar != null) {
                snackbar.dismiss();
                snackbar = null;
            }
            BeatsService.startService(context, pr.getName(), formatPlayerStatus(), false, false);
            if (player != null)
                player.pause();
            if (updatePlayer != null) {
                handler.removeCallbacks(updatePlayer);
                updatePlayer = null;
            }
            if (h != null)
                updatePlayerText(h, pr, position);
        }

        void playerStop() {
            if (snackbar != null) {
                snackbar.dismiss();
                snackbar = null;
            }
            BeatsService.stopService(context);
            if (updatePlayer != null) {
                handler.removeCallbacks(updatePlayer);
                updatePlayer = null;
            }
            if (player != null) {
                player.release();
                player = null;
            }
            playing = PLAYING_NONE;
        }

        void updatePlayerRun(final Program f, final int position) {
            if (playingView != null) // prevent reuse scroll / update view
                updatePlayerText(playingView, f, position);

            boolean playing = player != null && player.isPlaying();
            boolean end = player == null || player.isEnd();
            BeatsService.startService(context, f.getName(), formatPlayerStatus(), playing, end);

            if (updatePlayer != null) {
                handler.removeCallbacks(updatePlayer);
                updatePlayer = null;
            }

            if (!playing) {
                updateCustom();
                return;
            }

            updatePlayer = new Runnable() {
                @Override
                public void run() {
                    updatePlayerRun(f, position);
                }
            };
            handler.postDelayed(updatePlayer, PLAYER_UPDATE);
        }

        int getCount(Program pr) {
            Integer i = counts.get(pr);
            if (i == null)
                i = 0;
            return i;
        }

        void setCount(Program pr, int i) {
            counts.put(pr, i);
        }

        boolean updatePlayerText(final BeatsHolder h, final Program pr, final int position) {
            boolean playing = false;

            long c = 0;
            String s = "";

            if (player != null && Recordings.this.playing == position) {
                playing = player.isPlaying();
                c = player.getCurrentPosition();
                s = player.getStatus();
            }

            if (h.play != null)
                h.play.setImageResource(playing ? R.drawable.pause : R.drawable.play);
            if (h.ctrl != null) { // update status bar controls
                if (h.play2 != null)
                    h.play2.setImageResource(playing ? R.drawable.pause : R.drawable.play);

                h.loop_text.setText("x" + (getCount(pr) + 1));

                h.plus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int i = getCount(pr);
                        i = i + 1;
                        if (i >= 8)
                            i = 8;
                        setCount(pr, i);
                        updatePlayerText(h, pr, position);
                        toastDuration(pr.getLength() * 1000 * (i + 1));
                    }
                });

                h.minus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int i = getCount(pr);
                        i = i - 1;
                        if (i < 0)
                            i = 0;
                        setCount(pr, i);
                        updatePlayerText(h, pr, position);
                        toastDuration(pr.getLength() * 1000 * (i + 1));
                    }
                });
            }
            if (h.stop != null) // custom beats
                h.stop.setVisibility(player != null ? View.VISIBLE : View.INVISIBLE);

            if (h.bar != null) {
                h.bar.load(pr);
                h.bar.setDrawBackground(true);
                h.bar.setDrawBackgroundLimit(c);
                if (playing)
                    h.bar.setViewPort(0, pr.getLength());
                h.bar.invalidate();
            }

            if (s.isEmpty())
                s = BeatsApplication.formatDuration(context, pr.getLength() * 1000);
            h.status.setText(s);

            return playing;
        }

        void toastDuration(long time) {
            String s = BeatsApplication.formatDuration(context, time);
            if (player != null && player.isPlaying())
                s += " (" + HHMMSS.format(new Date(System.currentTimeMillis() + time - player.getCurrentPosition() * 1000)) + ")";
            if (toast != null)
                toast.cancel();
            toast = Toast.makeText(context, s, Toast.LENGTH_SHORT);
            toast.show();
        }

        public void select(int pos) {
            if (selected != pos && selected != -1)
                recordings.notifyItemChanged(selected);
            selected = pos;
            if (pos != -1)
                recordings.notifyItemChanged(pos);
        }

        @Override
        public int getItemViewType(int position) {
            Object o = list.get(position);
            if (o instanceof String)
                return 1;
            return 0;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }

    @Override
    public int getAppTheme() {
        return BeatsApplication.getTheme(this, R.style.AppThemeLight_NoActionBar, R.style.AppThemeDark_NoActionBar);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String a = intent.getAction();
                if (a == null)
                    return;
                if (a.equals(PAUSE_BUTTON)) {
                    if (recordings.playing == PLAYING_CUSTOM) {
                        if (recordings.player.isEnd()) {
                            playCustom();
                        } else {
                            recordings.playerPause();
                            updateCustom();
                        }
                    } else {
                        recordings.playerPause();
                        updateCustom();
                    }
                }
                if (a.equals(STOP_BUTTON)) {
                    recordings.playerStop();
                    recordings.notifyDataSetChanged();
                    updateCustom();
                }
            }
        };
        IntentFilter ff = new IntentFilter();
        ff.addAction(PAUSE_BUTTON);
        ff.addAction(STOP_BUTTON);
        registerReceiver(receiver, ff);

        screenreceiver = new ScreenReceiver() {
            @Override
            public void onScreenOff() {
                if (recordings.player == null)
                    return;
                super.onScreenOff();
            }
        };
        screenreceiver.registerReceiver(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab_panel = findViewById(R.id.fab_panel);

        fab_stop = findViewById(R.id.fab_stop);
        fab_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recordings.playerStop();
                updateCustom();
            }
        });

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (recordings.playing == PLAYING_CUSTOM) {
                    if (recordings.player.isEnd()) {
                        playCustom();
                    } else {
                        recordings.playerPause();
                        updateCustom();
                    }
                } else {
                    CustomDialog dialog = new CustomDialog();
                    Bundle args = createCustomArgs();
                    dialog.setArguments(args);
                    dialog.show(getSupportFragmentManager(), "");
                }
            }
        });

        recordings = new Recordings(this);
        recordings.scan();

        animator = new ExpandItemAnimator() {
            @Override
            public Animation apply(RecyclerView.ViewHolder h, boolean animate) {
                if (h instanceof CategoryHolder)
                    return null;
                int pos = h.getAdapterPosition();
                if (recordings.selected == pos)
                    return BeatsAnimation.apply(list, h.itemView, true, animate, recordings.playing == pos);
                else
                    return BeatsAnimation.apply(list, h.itemView, false, animate, recordings.playing == pos);
            }
        };

        View empty = findViewById(R.id.empty_list);
        list = (RecyclerView) findViewById(R.id.list);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setItemAnimator(animator);
        list.addOnScrollListener(animator.onScrollListener);
        recordings.empty.setEmptyView(empty);
        list.setAdapter(recordings.empty);
        list.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);

        if (shared.getBoolean(BeatsApplication.PREFERENCE_CALL, false)) {
            TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
            tm.listen(pscl, PhoneStateListener.LISTEN_CALL_STATE);
        }

        SeekBar sbg = (SeekBar) findViewById(R.id.volume_bg);
        sbg.setProgress((int) (BeatsApplication.getFlatBgVol(this) * 100));
        sbg.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float vol = progress / 100f;
                SharedPreferences.Editor edit = shared.edit();
                edit.putFloat(BeatsApplication.PREFERENCE_BG, vol);
                edit.apply();
                if (recordings.player != null)
                    recordings.player.setFlatBgVol(vol);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        SeekBar svc = (SeekBar) findViewById(R.id.volume_vc);
        svc.setProgress((int) (BeatsApplication.getFlatBtVol(this) * 100));
        svc.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float vol = progress / 100f;
                SharedPreferences.Editor edit = shared.edit();
                edit.putFloat(BeatsApplication.PREFERENCE_BT, vol);
                edit.apply();
                if (recordings.player != null)
                    recordings.player.setFlatBtVol(vol);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        updateCustom();

        if (OptimizationPreferenceCompat.needKillWarning(this, BeatsApplication.PREFERENCE_NEXT))
            OptimizationPreferenceCompat.buildKilledWarning(new ContextThemeWrapper(this, getAppTheme()), true, BeatsApplication.PREFERENCE_OPTIMIZATION).show();
        else if (OptimizationPreferenceCompat.needBootWarning(this, BeatsApplication.PREFERENCE_BOOT))
            OptimizationPreferenceCompat.buildBootWarning(this, BeatsApplication.PREFERENCE_BOOT).show();

        BeatsService.stopService(this); // happens when app killed by android
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar base clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
        if (id == R.id.action_test) {
            SoundTestDialog dialog = new SoundTestDialog();
            dialog.show(getSupportFragmentManager(), "");
            return true;
        }
        if (id == R.id.action_open) {
            choicer = new OpenChoicer(OpenFileDialog.DIALOG_TYPE.FILE_DIALOG, true) {
                @Override
                public void onResult(Uri uri) {
                    openDialog(uri);
                }
            };
            choicer.setPermissionsDialog(this, PERMISSIONS, RESULT_FILE);
            choicer.setStorageAccessFramework(this, RESULT_FILE);
            choicer.show(null);
        }
        if (id == R.id.action_about) {
            AboutPreferenceCompat.showDialog(this, R.raw.about);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case RESULT_FILE:
                if (choicer != null) // double call
                    choicer.onRequestPermissionsResult(permissions, grantResults);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RESULT_FILE:
                if (choicer != null) // double call
                    choicer.onActivityResult(resultCode, data);
                break;
        }
    }

    void openDialog(Uri u) {
        OpenDialog dialog = new OpenDialog();
        Bundle args = new Bundle();
        args.putParcelable("url", u);
        dialog.setArguments(args);
        dialog.show(getSupportFragmentManager(), "");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        handler.post(new Runnable() {
            @Override
            public void run() {
                list.smoothScrollToPosition(recordings.selected);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        recordings.close();

        if (pscl != null) {
            TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
            tm.listen(pscl, PhoneStateListener.LISTEN_NONE);
            pscl = null;
        }
        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }
        if (screenreceiver != null) {
            unregisterReceiver(screenreceiver);
            screenreceiver = null;
        }
    }

    @Override
    public void onStart() {
        try { // onCreateDialog exceptions
            super.onStart();
        } catch (RuntimeException e) {
            ErrorDialog.Error(this, e);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if (recordings.player != null)
            moveTaskToBack(true);
        else
            super.onBackPressed();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (dialog == null)
            return;

        if (dialog instanceof CustomDialog.Result) {
            CustomDialog.Result r = (CustomDialog.Result) dialog;

            final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor edit = shared.edit();
            edit.putInt(BeatsApplication.CUSTOM_BACKGROUND, r.background.ordinal());
            edit.putLong(BeatsApplication.CUSTOM_DURATION, r.duration);
            edit.putBoolean(BeatsApplication.CUSTOM_BEAT, r.beats);
            edit.putFloat(BeatsApplication.CUSTOM_BASE, r.base);
            edit.putFloat(BeatsApplication.CUSTOM_BEATSTART, r.beatStart);
            edit.putFloat(BeatsApplication.CUSTOM_BEATEND, r.beatEnd);
            edit.commit();

            playCustom();
        }

        if (dialog instanceof OpenDialog.Result) {
            OpenDialog.Result r = (OpenDialog.Result) dialog;
            if (r.done)
                playCustom(r.pr);
        }
    }

    Bundle createCustomArgs() {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        Bundle args = new Bundle();
        args.putLong("duration", shared.getLong(BeatsApplication.CUSTOM_DURATION, 60 * 60 * 1000 + 20 * 60 * 1000));
        args.putInt("background", shared.getInt(BeatsApplication.CUSTOM_BACKGROUND, SoundLoop.WHITE_NOISE.ordinal()));
        args.putBoolean("beats", shared.getBoolean(BeatsApplication.CUSTOM_BEAT, true));
        args.putFloat("base", getFloat(shared, BeatsApplication.CUSTOM_BASE, (float) Note.A_FREQ));
        args.putFloat("start", getFloat(shared, BeatsApplication.CUSTOM_BEATSTART, 20f));
        args.putFloat("end", getFloat(shared, BeatsApplication.CUSTOM_BEATEND, 60f));
        return args;
    }

    void playCustom() {
        Bundle args = createCustomArgs();

        Program pr = new Program(getString(R.string.group_mediation));
        Period p = new Period((int) (args.getLong("duration") / 1000), SoundLoop.values()[args.getInt("background")], CustomDialog.DEFAULT_BG_VOL);
        if (args.getBoolean("beats"))
            p.addVoice(new BinauralBeatVoice(args.getFloat("start"), args.getFloat("end"), BeatsPlayer.DEFAULT_VOLUME, args.getFloat("base")));
        pr.addPeriod(p);

        boostVolume(pr);

        playCustom(pr);
    }

    void playCustom(Program pr) {
        recordings.playerStop();
        recordings.notifyDataSetChanged(); // update preset views

        recordings.playerPlay(new BeatsHolder(fab_panel), pr, PLAYING_CUSTOM);

        updateCustom();
    }

    void updateCustom() {
        if (recordings.player == null || recordings.playing != PLAYING_CUSTOM) {
            fab_panel.setVisibility(View.GONE);
            fab.setImageResource(R.drawable.ic_access_alarm_black_24dp);
        } else {
            fab_panel.setVisibility(View.VISIBLE);
            if (recordings.player.isPlaying())
                fab.setImageResource(R.drawable.ic_pause_black_24dp);
            else
                fab.setImageResource(R.drawable.ic_play_arrow_black_24dp);
        }
    }
}
